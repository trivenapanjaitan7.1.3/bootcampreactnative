/*B. Tugas Conditional
If-else
Petunjuk : Kita akan memasuki dunia game werewolf. Pada saat akan bermain kamu diminta memasukkan nama dan peran . Untuk memulai game pemain harus memasukkan variable nama dan peran. Jika pemain tidak memasukkan nama maka game akan mengeluarkan warning “Nama harus diisi!“. Jika pemain memasukkan nama tapi tidak memasukkan peran maka game akan mengeluarkan warning “Pilih Peranmu untuk memulai game“. Terdapat tiga peran yaitu penyihir, guard, dan werewolf. Tugas kamu adalah membuat program untuk mengecek input dari pemain dan hasil response dari game sesuai input yang dikirimkan.

Petunjuk:

Nama dan peran diisi manual dan bisa diisi apa saja
Nama tidak perlu dicek persis sesuai dengan input/output
Buat kondisi if-else untuk masing-masing peran
Input:
*/
var nama = "John"
var peran = ""
/*Output:

// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"*/
var nama = ''
var peran = ''
if (nama=='' && peran =='')
    console.log("Nama harus diisi!");
console.log("\n");

//Output untuk Input nama = 'John' dan peran = ''
//"Halo John, Pilih peranmu untuk memulai game!"
var nama = 'John'
var peran = ''
if (nama!='' && peran =='')
console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
console.log("\n");

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
//"Selamat datang di Dunia Werewolf, Jane"
//"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
var nama = 'Jane'
var peran = 'Penyihir'
if (nama!='' && peran =='Penyihir'){
console.log("Selamat datang di Dunia Werewolf, "+nama)
console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");}
console.log("\n");

 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
//"Selamat datang di Dunia Werewolf, Jenita"
//"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
var nama = 'Jenita'
var peran = 'Guard'
if (nama!='' && peran =='Guard'){
console.log("Selamat datang di Dunia Werewolf, "+nama)
console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");}
console.log("\n");
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
//"Selamat datang di Dunia Werewolf, Junaedi"
//"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 
var nama = 'Junaedi'
var peran = 'Werewolf'
if (nama!='' && peran =='Werewolf'){
console.log("Selamat datang di Dunia Werewolf, "+nama)
console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!");}
console.log("\n");


/*Switch Case
Kamu akan diberikan sebuah tanggal dalam tiga variabel, yaitu hari, bulan, dan tahun. Disini kamu diminta untuk membuat format tanggal. Misal tanggal yang diberikan adalah hari 1, bulan 5, dan tahun 1945. Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.

Gunakan switch case untuk kasus ini!

Contoh:

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
Skeleton Code / Code yang dicontohkan yang perlu diikuti dan dimodifikasi

var tanggal; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)*/

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
var bulanString;


if ((tahun>=1900 && tahun<=2200) && (bulan>=1 && bulan<=12)){
    if ((bulan==1 || bulan==3 || bulan==5 || bulan==7 || bulan==8 || bulan==10 || bulan==12) && (hari>31||hari<1)) {
        console.log("Tanggal tidak valid")
    }
    if ((bulan==4 || bulan==6 || bulan==9 || bulan==11) && (hari>30||hari<1)) {
        console.log("Tanggal tidak valid")
    }
    if (bulan==2){
        if ((tahun%4==0 && tahun%100!=0) && (hari>29||hari<1))
            console.log("Tanggal tidak valid");    
        else if ((tahun%4==0 && tahun%100!=0) && (hari>28||hari<1))
            console.log("Tanggal tidak valid");
    }
    else{
        switch (bulan){
            case 1 : {bulanString="Januari"; break;}
            case 2 : {bulanString="Februari"; break;}
            case 3 : {bulanString="Maret"; break;}
            case 4 : {bulanString="April"; break;}
            case 5 : {bulanString="Mei"; break;}
            case 6 : {bulanString="Juni"; break;}
            case 7 : {bulanString="Juli"; break;}
            case 8 : {bulanString="Agustus"; break;}
            case 9 : {bulanString="September"; break;}
            case 10 : {bulanString="Oktober"; break;}
            case 11 : {bulanString="November"; break;}
            case 12 : {bulanString="Desember"; break;}
        }
        console.log(hari + " " + bulanString + " " + tahun); }
}
    else
        console.log("Tidak dapat menampilkan format tanggal")
    
// Menampilkan format tanggal
console.log("\n");