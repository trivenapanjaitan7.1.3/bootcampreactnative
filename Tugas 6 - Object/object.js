/*Tugas 6 – Object Literal 
 

Pada tugas kali ini buatlah folder baru (Tugas 6 – Object) yang berisikan file dengan nama object.js . Simpan folder tugas ini di dalam folder repository project BootcampReactNative yang dikerjakan pada folder kemarin .

Soal No. 1 (Array to Object)
Buatlah function dengan nama arrayToObject() yang menerima sebuah parameter berupa array multidimensi. Dalam array tersebut berisi value berupa First Name, Last Name, Gender, dan Birthyear. Data di dalam array dimensi tersebut ingin kita ubah ke dalam bentuk Object dengan key bernama : firstName, lastName, gender, dan age. Untuk key age ambillah selisih tahun yang ditulis di data dengan tahun sekarang. Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai : “Invalid birth year”.

Contoh: jika input nya adalah [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]

maka outputnya di console seperti berikut :

1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
Untuk mendapatkan tahun sekarang secara otomatis bisa gunakan Class Date dari Javascript.

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
Code
*/
console.log("Soal 1")
console.log("------")
function arrayToObject(arr) {
    // Code di sini 
    var arr0={
        firstName : arr[0][0],
        lastName : arr[0][1],
        gender : arr[0][2],
        age : arr[0][3]
    }
    var arr1={
        firstName : arr[1][0],
        lastName : arr[1][1],
        gender : arr[1][2],
        age : arr[1][3]
    }
    
    var now = new Date()
    var thisYear = now.getFullYear() // 2021 (tahun sekarang)
    
    if (arr0.age>thisYear || arr0.age==null)
        arr0.age="Invalid birth year";
    if (arr1.age>thisYear || arr1.age==null)
        arr1.age="Invalid birth year";
    if (arr0.age<=thisYear){
        var age=arr0.age;
        arr0.age=thisYear-age;
    }
    if (arr1.age<=thisYear){
        var age=arr1.age;
        arr1.age=thisYear-age;
    }
    console.log("1. "+arr0.firstName+" "+arr0.lastName+": { firstName: “"+arr0.firstName+"”, lastName: “"+arr0.lastName+"”, gender: “"+arr0.gender+"”, age: “"+arr0.age+"}")
    console.log("2. "+arr1.firstName+" "+arr1.lastName+": { firstName: “"+arr1.firstName+"”, lastName: “"+arr1.lastName+"”, gender: “"+arr1.gender+"”, age: “"+arr1.age+"}")
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
//arrayToObject([]) // ""



/*Soal No. 2 (Shopping Time)
Problem

Diberikan sebuah function shoppingTime(memberId, money) yang menerima dua parameter berupa string dan number. Parameter pertama merupakan memberIddan parameter ke-2 merupakan value uang (money) yang dibawa oleh member tersebut.

Toko X sedang melakukan SALE untuk beberapa barang, yaitu:

Sepatu brand Stacattu seharga 1500000
Baju brand Zoro seharga 500000
Baju brand H&N seharga 250000
Sweater brand Uniklooh seharga 175000
Casing Handphone seharga 50000
Buatlah function yang akan mengembalikan sebuah object dimana object tersebut berisikan informasi memberId, money, listPurchased dan changeMoney.

Jika memberId kosong maka tampilkan “Mohon maaf, toko X hanya berlaku untuk member saja”
Jika uang yang dimiliki kurang dari 50000 maka tampilkan “Mohon maaf, uang tidak cukup”
Member yang berbelanja di toko X akan membeli barang yang paling mahal terlebih dahulu dan akan membeli barang-barang yang sedang SALE masing-masing 1 jika uang yang dimilikinya masih cukup.
Contoh jika inputan memberId: ‘324193hDew2’ dan money: 700000

maka output:

{ memberId: ‘324193hDew2’, money: 700000, listPurchased: [ ‘Baju Zoro’, ‘Sweater Uniklooh’ ], changeMoney: 25000 }

Code
*/
console.log("\n")
console.log("=====================================================================================")
console.log("Soal 2")
console.log("------")

function shoppingTime(memberId0, money0) {
  // you can only write your code here!
  var listPurchased0 = []
  var changeMoney0 = money0
  var barang = [["Sepatu Stacattu",1500000],["Baju Zoro",500000],["Baju H&N",250000],["Sweater Uniklooh",175000],["Casing Handphone",50000]]

    if (memberId0==null || memberId0=='')
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    else {
        if (money0<50000)
            return "Mohon maaf, uang tidak cukup";
        else{
            if (money0>=barang[0][1]){
                changeMoney=changeMoney0-barang[0][1];
                listPurchased0[0]=barang[0][0];
            }
            if (money0>=barang[1][1]){
                changeMoney=changeMoney0-barang[1][1];
                if (listPurchased0[0]!=null)
                    listPurchased0[1]=barang[1][0];
                else
                    listPurchased0[0]=barang[1][0];
            }
            if (money0>=barang[2][1]){
                changeMoney=changeMoney0-barang[2][1];
                if (listPurchased0[1]!=null)
                    listPurchased0[2]=barang[2][0];
                else
                    listPurchased0[0]=barang[2][0];
            }
            if (money0>=barang[3][1]){
                changeMoney=changeMoney0-barang[3][1];
                if (listPurchased0[2]!=null)
                    listPurchased0[3]=barang[3][0];
                else
                    listPurchased0[0]=barang[3][0];
            }
            if (money0>=barang[4][1]){
                changeMoney=changeMoney0-barang[4][1];
                if (listPurchased0[3]!=null)
                    listPurchased0[4]=barang[4][0];
                else
                    listPurchased0[0]=barang[4][0];
            }
        }
    }
    var data = {
        memberId : memberId0,
        money : money0,
        listPurchased : listPurchased0,
        changeMoney : changeMoney0,
    }

    return data;
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



/*Soal No. 3 (Naik Angkot)
Problem

Diberikan function naikAngkot(listPenumpang) yang akan menerima satu parameter berupa array dua dimensi. Function akan me-return array of object.

Diberikan sebuah rute, dari A – F. Penumpang diwajibkan membayar Rp2000 setiap melewati satu rute.

Contoh: input: [ [‘Dimitri’, ‘B’, ‘F’] ] output: [{ penumpang: ‘Dimitri’, naikDari: ‘B’, tujuan: ‘F’, bayar: 8000 }]

Code
*/

console.log("\n")
console.log("=====================================================================================")
console.log("Soal 3")
console.log("------")

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  var x1=0,y1=0,x2=0,y2=0;
  var bayar = 2000;
  for(var i=0; i<rute.length; i++){    
    if (arrPenumpang[0][1]==rute[i])
        x1=i;
    if (arrPenumpang[0][2]==rute[i])
        y1=i;
    if (arrPenumpang[1][1]==rute[i])
        x2=i;
    if (arrPenumpang[1][2]==rute[i])
        y2=i;
  }
  
  var arr = [{
        penumpang : arrPenumpang[0][0],
        naikDari : arrPenumpang[0][1],
        tujuan : arrPenumpang[0][2],
        bayar : bayar*(y1-x1)
        }, {
        penumpang : arrPenumpang[1][0],
        naikDari : arrPenumpang[1][1],
        tujuan : arrPenumpang[1][2],
        bayar : bayar*(y2-x2)
        }
    ]
    return arr;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
//console.log(naikAngkot([])); //[]