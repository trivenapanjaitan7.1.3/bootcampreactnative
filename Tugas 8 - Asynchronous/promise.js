/**Tugas - 8 - Asynchronous
Menggunakan callback dan promise

Pada tugas kali ini buatlah folder baru (Tugas 8 – Asynchronous) yang berisikan 4 file dengan nama index.js, callback.js, index2.js dan promise.js. Simpan folder tugas ini di dalam folder repository project yang dikerjakan pada Folder BootcampReactNative kemarin.

Soal No. 1 (Callback Baca Buku)
Kita mempunyai tumpukan buku untuk dibaca. Setiap buku memiliki waktu yang dibutuhkan untuk menghabiskan buku tersebut. Sudah disediakan function readBooks yang menerima tiga parameter: waktu, buku yang dibaca, dan sebuah callback. Salin code berikut ke dalam sebuah file bernama callback.js .
 */

function readBooksPromise (time, book) {
    console.log(`saya mulai membaca ${book.name}`)
    return new Promise( function (resolve, reject){
      setTimeout(function(){
        let sisaWaktu = time - book.timeSpent
        if(sisaWaktu >= 0 ){
            console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            resolve(sisaWaktu)
        } else {
            console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
            reject(sisaWaktu)
        }
      }, book.timeSpent)
    })
  }
   
  module.exports = readBooksPromise
