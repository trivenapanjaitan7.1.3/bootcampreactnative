/*Tugas - 8 - Asynchronous
Menggunakan callback dan promise

Pada tugas kali ini buatlah folder baru (Tugas 8 – Asynchronous) yang berisikan 4 file dengan nama index.js, callback.js, index2.js dan promise.js. Simpan folder tugas ini di dalam folder repository project yang dikerjakan pada Folder BootcampReactNative kemarin.

Soal No. 1 (Callback Baca Buku)
Kita mempunyai tumpukan buku untuk dibaca. Setiap buku memiliki waktu yang dibutuhkan untuk menghabiskan buku tersebut. Sudah disediakan function readBooks yang menerima tiga parameter: waktu, buku yang dibaca, dan sebuah callback. Salin code berikut ke dalam sebuah file bernama callback.js .

*/
function readBooks (time, book, callback)  {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let sisaWaktu=0
        if(time>book.timeSpent){
            sisaWaktu=time-book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) // menjalankan function callbak
        } else{
            console.log('waktu saya habis')
            callback(time)
        }
    }, book.timeSpent)
}

module.exports=readBooks

//Masih satu folder dengan file callback.js, buatlah sebuah file dengan nama index.js lalu tuliskan code seperti berikut.
